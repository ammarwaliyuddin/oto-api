from flask import Flask, jsonify,request 
from math import *
import re
import json
import mysql.connector

app = Flask(__name__)

def get_unique_names_within_area(data):
    
    # Membuka koneksi ke database MySQL
    connection = mysql.connector.connect(
        host='localhost',
        user='root',
        password='Welcomeaug33',
        database='oto_dev'
    )

    # Membuat objek cursor
    cursor = connection.cursor()

    # Mengeksekusi query untuk mengambil data dari tabel
    query = "SELECT * FROM m_data_tol"
    cursor.execute(query)

    # Mendapatkan semua hasil data
    dataSet = cursor.fetchall()

    # Buka file JSON
    # with open('data_testing/data_testing_f.json', 'r') as file:
    #     # Memuat data dari file JSON
    #     data = json.load(file)

    # Mendefinisikan set untuk menyimpan nama gerbang unik
    unique_names = set()

    def dms_to_decimal(dms_coord):
        parts = re.split(r'[°\'"\s]', dms_coord)
        degrees = float(parts[0])
        minutes = float(parts[1])
        seconds = float(parts[2])
        direction = parts[3]
        
        decimal = degrees + minutes/60 + seconds/3600
        if direction in ['S', 'W']:
            decimal = -decimal
        return decimal

    def km_to_m(kilometer):
        meter = kilometer * 1000
        return meter

    def haversine(lon1, lat1, lon2, lat2):
        
        # convert decimal degrees to radians
        lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

        # haversine formula
        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
        c = 2 * asin(sqrt(a))
        r = 6371 # Radius of earth in kilometers. Use 3956 for miles
        return c * r

    # Melakukan looping dan mencetak data
    for item in data:
        # Mencetak data
        for row in dataSet:
            center_point = [{'lat': item["latitude"], 'lng': item["longitude"]}]
            test_point = [{'lat': dms_to_decimal(row[2]), 'lng': dms_to_decimal(row[3])}]

            lat1 = center_point[0]['lat']
            lon1 = center_point[0]['lng']
            lat2 = test_point[0]['lat']
            lon2 = test_point[0]['lng']

            km = haversine(lon1, lat1, lon2, lat2)

            area = 0.008 # in kilometer
            if km <= area:
            	unique_names.add(row[1])

	

    # Menutup cursor dan koneksi ke database MySQL
    cursor.close()
    connection.close()

    return unique_names
 
@app.route('/')
def hello():
    return 'Hello, World!'

@app.route('/api/toll', methods=['POST'])
def toll():
    data = request.json['data']
    final_data = []
    data_toll = list(get_unique_names_within_area(data))
    # print(data_toll)
    for item in data_toll:
        final_data.append({"nama_gerbang": item})

    result_json = jsonify(final_data)
    return result_json

if __name__ == '__main__':
   app.run(debug=True, host='0.0.0.0')
